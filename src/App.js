import axios from "axios";
import {useEffect, useState} from "react";
import "./App.css";
import Header from "./components/Header";
import PunkList from "./components/PunkList";
import "./styles.css";

export default function App() {
  const [punkListData, setPunkListData] = useState([]);

  const createCORSRequest = (method, url) => {
    let xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      // Kiểm tra XMLHttpRequest object có thuộc tính
      // withCredentials hay không
      // Thuộc tính này chỉ có ở XMLHttpRequest2
      xhr.open(method, url, true);
    } else if (typeof XDomainRequest != "undefined") {
      // Kiểm tra XDomainRequest
      // Đây là đối tượng chỉ có ở IE và
      // là cách để IE thực hiện truy vấn CORS
      xhr = new XDomainRequest();
      xhr.open(method, url);
    } else {
      xhr = null;
    }
    return xhr;
  };

  const getMyNfts = async () => {
    const openseaData = await axios.get(
      "https://testnets-api.opensea.io/assets?asset_contract_address=0xf6679ec31d37f5b07338490eF72e9753885f8417&order_direction=asc"
    );
    console.log(openseaData.data.assets);
    const request = createCORSRequest(
      "GET",
      "https://testnets-api.opensea.io/assets?asset_contract_address=0xf6679ec31d37f5b07338490eF72e9753885f8417&order_direction=asc"
    );
    console.log(request);
    if (!request) {
      throw new Error("CORS is not supported");
    }
    setPunkListData(openseaData.data.assets);
  };

  useEffect(() => {
    getMyNfts();
  }, []);

  return (
    <div className='app'>
      <Header />

      <PunkList punkListData={punkListData} />
    </div>
  );
}
