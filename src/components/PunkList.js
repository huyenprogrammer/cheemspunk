import React from "react";
import CollectionCard from "./CollectionCard";
import "./PunkList.css";

export default function PunkList({punkListData}) {
  return (
    <div className='punkList'>
      {punkListData.map((item) => (
        <div key={item.token_id}>
          <CollectionCard
            id={item.token_id}
            name={item.name}
            traits={item.traits}
            image={item.image_original_url}
          />
        </div>
      ))}
    </div>
  );
}
